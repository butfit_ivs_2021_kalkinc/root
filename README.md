## Instalace

Automatická instalace:
Poklepáním na soubor instalátoru .deb se otevře softwarové centrum ubuntu, kde je možné aplikaci jedním stisknutím tlačítka nainstalovat i odinstalovat. Aplikaci lze poté spustit přes spouštěč pod názvem "Kalk Inc. App" nebo skrz terminál pomocí *kalkincapp*.

Ruční instalace: 
Aplikaci přeložíme z jejího zdrojového adresáře pomocí příkazu *./gradlew jar*; tento si automaticky i natáhne knihovnu již předkompilovanou. Zkompilovaný soubor *app.jar* z adresář *app/build/libs* ve zdrojovém adresáři zkopírujeme do adresáře */usr/share/kalkincapp/* a vytvoříme zde i ikonku aplikace, v adresáři */usr/bin* poté vytvoříme shellový skript spouštějící tento soubor prostřednictvím javy s odpovídajícími argumenty. V adresáři */usr/share/applications* nakonec vytvoříme .desktop soubor pro pohodlné spouštění aplikace.

## Prostředí

Windows 64bit

## Autoři

Kalk Inc.

- xditej01 Jindřich Dítě
- xlebed11 Anastasiia Lebedenko
- xcuhan00 Jakub Čuhanič
- xhradi19 Michal Hradil

## Licence

Tento program je poskytován pod licencí MIT, viz licence v adresářích jednotlivých projektů.
